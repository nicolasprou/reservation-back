const express = require("express");
bodyParser = require("body-parser");
const app = express();
const port = 3000;
const cors = require("cors");
app.use(
  cors({
    origin: "http://localhost:8080",
  })
);
app.use(bodyParser.json());

const fs = require("fs");

let reservations = fs.readFileSync("./json/reservations.json");
let salles = fs.readFileSync("./json/salles.json");

app.get("/reservations", cors(), (req, res) => {
  res.send(JSON.parse(reservations));
});

app.post("/reservations", cors(), (req, res) => {
  fs.writeFile("./json/reservations.json", JSON.stringify(req.body), (err) => {
    if (err) {
      console.log("Error writing file", err);
    } else {
      console.log("Successfully wrote file");
    }
  });
});

app.post("/annuler-reservations", cors(), (req, res) => {
  fs.writeFile("./json/reservations.json", JSON.stringify(req.body), (err) => {
    if (err) {
      console.log("Error writing file", err);
    } else {
      console.log("Successfully wrote file");
      res.send(req.body);
    }
  });
});

app.get("/salles", cors(), (req, res) => {
  res.send(JSON.parse(salles));
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
